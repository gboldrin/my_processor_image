FROM gitlab-registry.cern.ch/linuxsupport/alma9-base:latest

ADD docker-files/batch9-stable.repo /etc/yum.repos.d/

RUN yum -y update \
	&& yum clean all

RUN yum -y install myschedd krb5-workstation ngbauth-submit perl-Sys-Syslog git curl bzip2 which \
	&& yum clean all

ADD docker-files/ngauth_batch_crypt_pub.pem /etc/
ADD docker-files/ngbauth-submit /etc/sysconfig/
ADD docker-files/myschedd.yaml /etc/myschedd/
# we need a functional krb5.conf and this one comes from ngbauth-submit
RUN rm -f /etc/krb5.conf
RUN ln -s /etc/krb5.conf.no_rdns /etc/krb5.conf



WORKDIR /tmp/

# RUN curl -L -O "https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-Linux-x86_64.sh" \
# 	&& bash Miniforge3-Linux-x86_64.sh -bfp /usr/local \
# 	&& rm -f Miniforge3-Linux-x86_64.sh

RUN curl -sSL "https://github.com/conda-forge/miniforge/releases/download/23.3.1-1/Mambaforge-Linux-x86_64.sh" -o mambaforge.sh \
	&& bash mambaforge.sh -bfp /usr/local \
	&& rm -f mambaforge.sh \
	&& mamba update mamba \
	&& mamba clean --all --yes

ADD docker-files/env.yaml env.yaml

RUN mamba env update --file env.yaml \
	&& mamba clean --all -f -y


WORKDIR /

RUN mkdir -p /usr/local/etc/condor/config.d
ADD docker-files/condor_submit.config /usr/local/etc/condor/config.d/

# but config still looked for in /etc/condor for some reason
RUN mkdir -p /etc/condor/
ADD docker-files/condor_config /etc/condor/
RUN mkdir -p /etc/condor/config.d
ADD docker-files/condor_submit.config /etc/condor/config.d/



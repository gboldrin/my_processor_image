

Run with 
```bash
apptainer pull docker://gitlab-registry.cern.ch/gpizzati/my_processor_image:latest
# maybe remove ~/.apptainer that is used as default cache dir and could store ~ 1.5 GB

apptainer shell -B /afs -B /cvmfs -B /eos -B ${XDG_RUNTIME_DIR}  --env KRB5CCNAME=${XDG_RUNTIME_DIR}/krb5cc my_processor_image_latest.sif


# check that condor is accessible
Apptainer> condor_q

-- Schedd: XXXXXXXXXXXXXXXXX : <XXXXXXXXXXXXXXXXX?... @ 04/27/24 20:43:03
OWNER BATCH_NAME      SUBMITTED   DONE   RUN    IDLE   HOLD  TOTAL JOB_IDS

Total for query: 0 jobs; 0 completed, 0 removed, 0 idle, 0 running, 0 held, 0 suspended
Total for gpizzati: 0 jobs; 0 completed, 0 removed, 0 idle, 0 running, 0 held, 0 suspended
Total for all users: 36225 jobs; 130 completed, 1531 removed, 10 idle, 284 running, 34270 held, 0 suspended

```
